package com.example.modulenineparttwo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class AuthorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);
    }

    public void website(View view) {
        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/id222195112"));

        startActivity(intent);
    }
}
