package com.example.modulenineparttwo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText mPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPhoneNumber = findViewById(R.id.phoneNumber);
    }

    public void call(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel: " + mPhoneNumber.getText().toString()));
        startActivity(intent);

    }

    public void authorActivity(View view) {
        Intent intent = new Intent(MainActivity.this, AuthorActivity.class);
        startActivity(intent);
    }
}
