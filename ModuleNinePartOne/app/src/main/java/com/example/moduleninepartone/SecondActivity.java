package com.example.moduleninepartone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

public class SecondActivity extends AppCompatActivity {
    private EditText mNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mNumber = findViewById(R.id.number);
    }

    public void onClick(View view) {
        Intent intent =  new Intent(SecondActivity.this, ThirdActivity.class);
        String text = mNumber.getText().toString();
        if(!text.equals("")) {
            intent.putExtra("inputNumber", mNumber.getText().toString());
            startActivity(intent);
        } else {
            TextInputLayout textInputLayout = findViewById(R.id.textInputLayout);
            textInputLayout.setError("Enter a number");
        }
    }
}
