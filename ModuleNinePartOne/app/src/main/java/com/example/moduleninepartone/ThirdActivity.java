package com.example.moduleninepartone;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ThirdActivity extends AppCompatActivity {
    private TextView mInputView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        mInputView = findViewById(R.id.inputNumber);
        int inputNumber = Integer.parseInt(getIntent().getExtras().getString("inputNumber"));
        mInputView.setText(regainRelationship(inputNumber));
    }

    private String regainRelationship(int number){

        if(number > 100){
            return "Your number so big";
        } else if (number < 100){
            return "Your number so small";
        }

        return "My congratulations!";
    }
}
